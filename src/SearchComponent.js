import React from 'react';
import './Search.less';
import { MdSearch } from "react-icons/md";
import { MdKeyboardVoice } from "react-icons/md";

class SearchComponent extends React.Component{
    constructor(props, context) {
        super(props, context);
        this.state = {
            content: ""
        };
        this.onMyInputChange = this.onMyInputChange.bind(this);
        this.onButtonClicked = this.onButtonClicked.bind(this);
    }
    onMyInputChange(e) {
        this.setState({content: e.target.value});
    }
    onButtonClicked(e) {
        // console.log(this.state.content);
    }
    render() {
        return (
            <div className="searchComponent">
                <MdSearch className="searchIcon"/>
                <input onChange={this.onMyInputChange}/>
                <MdKeyboardVoice className="voiceIcon"/>
                {/*<h1>{this.state.content}</h1>*/}
                {/*<button onClick={this.onButtonClicked}></button>*/}
            </div>

        );
    }
}
export default SearchComponent;

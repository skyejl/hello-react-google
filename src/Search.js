import React from 'react';
import './Search.less';
import SearchComponent from './SearchComponent';

const Search = () => {
  return (
    <section className='search'>
      <header>
        <img src="https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png"
                   alt="Google Logo"/>
      </header>
        <SearchComponent/>
        <section className="search-button">
            <button className="button-item">Google Search</button>
            <button className="button-item">I'm really lucky</button>
        </section>

    </section>
  );
};

export default Search;
